package juego;

import java.util.ArrayList;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class MovimientoEntidades {


	private int contadorTickObstaculos = 0;
	private boolean esperarObstaculos = false;
	
	private int contadorTickItems = 0;
	private boolean esperarItems = false;

	private int contadorTickSoldados = 0;
	private boolean esperarSoldados = false;
	
	
 
	
	public  void moverObstaculos(ArrayList<Obstaculo> obstaculos, Entorno entorno,  Princesa princesa, 
			float VELOCIDAD_NIVEL) {
		
		
		Random rn = new Random();
	 
		

		for(int i = 0; i < obstaculos.size(); i++) {
		
			// esperar a que contador tick llege a 0, para volver a empezar 
			// con un tiempo de espera aleatoreo del proximo soldado. 
				if(esperarObstaculos == false) {
					esperarObstaculos = true;
				
			
					
					contadorTickObstaculos = randomTime();
				
				
				
				}else {
					contadorTickObstaculos--;		// esperamos.....
				}
				
				// enviamos un obstaculo al terminar el tiempo de espera
 				if(contadorTickObstaculos == 0) {
					
					// hacemos otra nueva esperar
					esperarObstaculos = false;
				 
			        obstaculos.get(i).setMove(true);
				 	
					 
				}
			
			
			
			obstaculos.get(i).mover(VELOCIDAD_NIVEL);
			obstaculos.get(i).dibujar(entorno);
			

			// si esta fuera del nivel lo eliminamos, y creamos uno nuevo, 
			// con doferente sprite
			if(obstaculos.get(i).getX()  < - obstaculos.get(i).getAncho() / 2 ) {
				
				
				int movilONormal = rn.nextInt(2);
				String [] randomObstaculo = {"fijo", "movil"};
				// enviamos uno fijo o normal
				Obstaculo nuevo = new Obstaculo(princesa, false, entorno, randomObstaculo[movilONormal]);
				obstaculos.remove(i);
				obstaculos.add(i, nuevo);	
			}
		}
	}
	
	
	
	 
	
	public  void moverItems(ArrayList<Item> item, Entorno entorno, Princesa princesa, 
			float VELOCIDAD_NIVEL) {

		
		

		for(int i = 0; i < item.size(); i++) {
		
			// esperar a que contador tick llege a 0, para volver a empezar 
			// con un tiempo de espera aleatoreo del proximo soldado. 
				if(esperarItems == false) {
					esperarItems = true;
				
			
					
					contadorTickItems = randomTime();
				
				
				
				}else {
					contadorTickItems--;		// esperamos.....
				}
				
				// cuando llege a 0 , movemos el proximo soldadito
				if(contadorTickItems == 0) {
					
					// hacemos otra nueva esperar
					esperarItems = false;
					
					item.get(i).setMove(true);
					
					
				}
			
			
			
				item.get(i).mover(VELOCIDAD_NIVEL);
				item.get(i).dibujar(entorno);
				
				
				// si se lale del nivel lo eliminamos, y creamos uno nuevo.
				if(item.get(i).getX() < - item.get(i).getAncho() / 2 ) {
					
					Item nuevo = new Item(entorno.ancho()+100, princesa, false, entorno);
					Random rn = new  Random();
					int variar = rn.nextInt(50);
					nuevo.setY(entorno.alto()/2+variar);// entre 100  y 400; );
					item.remove(i);
					item.add(i, nuevo);
					
				}
		}
	}
	

	
	public  void movimientoSoldados(ArrayList<Soldado> soldados, 
			Entorno entorno, Princesa princesa, Juego juego, 
			int velocidadSoldados, int velocidadSoldadosInicial) {

		// si esta fuera dle nivel lo eliminamos
		for(int i = 0; i < soldados.size(); i++) {
			
		}
		
		
		for(int i = 0; i < soldados.size(); i++) {
		 
			
			// esperar a que contador tick llege a 0, para volver a empezar 
		// con un tiempo de espera aleatoreo del proximo soldado. 
			if(esperarSoldados == false) {
				esperarSoldados = true;
				contadorTickSoldados = randomTime();
			}else {
				contadorTickSoldados--;		// esperamos.....
			}
			
			// cuando llege a 0 , movemos el proximo soldadito
			if(contadorTickSoldados == 0) {
				
				// hacemos otra nueva esperar
				esperarSoldados = false;
			
			     soldados.get(i).setMove(true);
				
			
			}
		
			
			
			soldados.get(i).mover();
			soldados.get(i).dibujar(entorno);
			
			if(soldados.get(i).getX()  < 0 - soldados.get(i).getAncho() / 2 ) {
				
				Soldado nuevoSoldado = new Soldado(entorno, princesa, juego, velocidadSoldados, velocidadSoldadosInicial);
				nuevoSoldado.setMove(false);
				soldados.remove(i);
				soldados.add(i, nuevoSoldado);
			}
		}
	}
	
	public  int  randomTime() {
		
		 int []randomTimes = new int[4];
		 
		
			randomTimes[0] = 200; 
			randomTimes[1] = 400; 
			randomTimes[2] = 600;
			randomTimes[3] = 700;
		
			

		 Random rn = new Random();
		 int tick = rn.nextInt(randomTimes.length);
		 
	

		 return randomTimes[tick];
		
	}

	

	
	
	
	
}
