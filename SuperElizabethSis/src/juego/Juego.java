package juego;



import java.util.ArrayList;
import java.util.Random;

import entorno.Entorno;

import entorno.InterfaceJuego;


public class Juego extends InterfaceJuego {

	    
private int nivel = 1;
private  boolean start_game = false;
		
private  Princesa princesa;
private   float velocidadMovimiento = 3;
	
private  int PUNTAJE_PARA_SIGUIENTE_NIVEL = 1000;
private  int puntajePorSoldado = 50;
private  int puntajePorItem = 30;
private   int vidas_totales = 3;
private   int puntaje = 0;
private  boolean GAME_OVER = false;

private int velocidadNivel = 3;
		
private  int TIEMPO_REUTILIZACION_DISPARO = 40; // 120seg=>1sg=>40sg==1/4
private boolean startDelayDisparo = false;
private  int delayDisparo = 0;
private int posicionInicialPrincesaX = 50;
private int posicionInicialPrincesaY = 462;
	    
	    
private Interfaz interfaz;
	    
private Colisiones  colisiones; 
private MovimientoEntidades movimientoEntidades;
	    

private Sonido sonido; 


private   int velocidadSoldados = 4;
private  int velocidadSoldadosInicial = velocidadSoldados;	
private   int cantidadObstaculosVisibles = 3;
private   int maximoDisparosVisibles = 3;

private   int cantidadTotalSoldadosVisibles = 6;
private   int cantidadTotalItemsVisibles = 6;	
private  int velocidadNivelInicial = velocidadNivel;
private  int nivelesMaximos = 3;

	    
private  boolean siguiente_nivel = false;

private boolean reiniciarNivel = false;
private int posicionYObstaculos; 



		 
// fondos
 // obstaculos
private   ArrayList <Obstaculo> obstaculos = new ArrayList<Obstaculo>();

 

// soldados
private ArrayList <Soldado> soldados = new ArrayList<Soldado>();
	 // items
private ArrayList <Item> items = new ArrayList<Item>();



private ArrayList <BolaFuego> bolasDeFuego = new ArrayList<BolaFuego>();	

private Entorno entorno;
private Background fondoNivel; 
private Background suelo1; 
private Background suelo2; 

 
    
 
	

	 Juego()
	 	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 17 - v1", 800, 600);
		this.entorno.setSize(800, 600);
		
		
		
		this.colisiones = new Colisiones(); // detecta colisiones
		this.princesa=new Princesa(50,462, entorno,  this); // jugador
		this.sonido = new Sonido();
		
		this.movimientoEntidades = new MovimientoEntidades(); 

	    // UI, informacion, letras, game over, puntos, vidas etc
 		this.interfaz = new Interfaz(entorno.ancho() / 20 ,
				(entorno.alto() / 10), entorno,   this);// Ui grafica
		
	
	
		sonido.reproducirMusicaFondo();
		
		 
		
		fondoNivel = new Background(entorno.ancho() /2 , entorno.alto() /2, entorno, getNivel(), "fondo");
		suelo1 =  new Background(entorno.ancho() /2 , entorno.alto() /2, entorno, getNivel(), "suelo");
		suelo2 =  new Background(suelo1.getX() + suelo1.getImagen().getWidth(null)  , this.entorno.alto()/2, entorno, getNivel(), "suelo");

		
		
		//Inicializar lo que haga falta para el juego
		
		crearEntidadesVisibles();
 

		// Inicia el juego!
		this.entorno.iniciar();
	}

	 
	 public void crearEntidadesVisibles() {
		 
		 this.posicionYObstaculos = princesa.getY();

			for(int i = 0; i < cantidadTotalSoldadosVisibles; i++) {
	
					// comitiar esto// +200 para que se vaya un poco mas adelante
				 Soldado soldado=new Soldado(entorno, princesa, this,  velocidadSoldados, velocidadSoldadosInicial);

				this.soldados.add(soldado);


			}
			;

			Random rn = new Random();
			for(int i  = 0; i < cantidadObstaculosVisibles; i ++) {

				
				int movilONormal = rn.nextInt(2);
				String [] randomObstaculo = {"fijo", "movil"};
			    Obstaculo  obstaculo = new Obstaculo( princesa,  false, entorno, randomObstaculo[movilONormal]);	
			
			
				obstaculos.add(obstaculo); 
				 
			}
			
			
		
		
			
			for(int i  = 0; i < cantidadTotalItemsVisibles; i ++) {

				
			    Item  item = new Item(entorno.ancho()+100, princesa,  false, entorno);	
			
			
			    items.add(item); 
				 
			}
			
		 
	 }
	 
	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	
	
	// se ejecuta 120 veces en 1 seg, aproximadamente
	// debemos dibujar en cada frame, el contenido del juego, 
	// cuando corresponda
	
	
	
	public void tick()
	{
		
		if(empezarJuego()) {
			dibujarFondo();
            controlDeNiveles();
            crearSuelo();
 			controlYDibujadoDeNuestroPersonaje();
			movimientoDeObjetos();
	
			// solo habra colisiones, mientras no hayamos completado el nivel
			if(puntaje < PUNTAJE_PARA_SIGUIENTE_NIVEL)
				colisiones();
			habilidadDeBolaDeFuego();	
			logicaDeMuerte();
			juegoGanadoOPerdido(); 
		 
	
	       this.interfaz.DibujarInterfaz(puntaje, vidas_totales);
		}	

	}
	
	
	public void dibujarFondo() {
		if(fondoNivel!=null)
			fondoNivel.dibujar();
	}
	
	
	public void crearSuelo() {
		
		if(suelo1 != null) {
			
			suelo1.dibujar();
			suelo1.mover(getVelocidadNivel());
		if(suelo1.getX() + suelo1.getImagen().getWidth(null) / 2 < 0) {
			suelo1 = null;
			suelo1 = new Background(entorno.ancho() + suelo2.getImagen().getWidth(null)/2, 
		 			entorno.alto()/2, entorno, getNivel(), "suelo");
			}
		}
		if(suelo2 != null) {
			suelo2.dibujar();
			suelo2.mover(getVelocidadNivel());
			if(suelo2.getX() + suelo2.getImagen().getWidth(null) / 2 < 0) {
				suelo2 = null;
				suelo2 = new Background(entorno.ancho() + suelo1.getImagen().getWidth(null)/2, 
			 			entorno.alto()/2, entorno, getNivel(), "suelo");
				}
			}
	}
	
	 // trasladamos objetos a su posicion inicial al perder una vida
	// o pasamos al siguiente nivel
	private void reiniciarObjetos() {
		
		// si se salio de la pantalla desde la coordenada y
		if((princesa.getY() + princesa.getAlto() / 2) 
				> entorno.alto()+(entorno.alto() / 2) || 
				isSiguiente_nivel() == true) {
			
			
			
			
			// ELIMINAR TODO
			soldados.clear();
			obstaculos.clear();
			items.clear();
			bolasDeFuego.clear();
			
			setVelocidadNivel(getVelocidadNivelInicial());
			
			
			
			setReiniciarNivel(false);
			princesa.setY(posicionInicialPrincesaY);
			princesa.setX(posicionInicialPrincesaX);
			princesa.setEstaMuerta(false);
			
			
			fondoNivel = null;
			suelo1 = null;
			suelo2 = null;
			fondoNivel = new Background(entorno.ancho() /2 , entorno.alto() /2, entorno, getNivel(), "fondo");
			suelo1 =  new Background(entorno.ancho() /2 , entorno.alto() /2, entorno, getNivel(), "suelo");
			suelo2 =  new Background(suelo1.getX() + suelo1.getImagen().getWidth(null)  , this.entorno.alto()/2, entorno, getNivel(), "suelo");
			
			crearEntidadesVisibles(); // RECREAMOS NIVEL
			 
			//
			if(isSiguiente_nivel()) {
				setSiguiente_nivel(false);
			}
		}
	}
	
	private boolean empezarJuego() {
		// dibujamos presentacion
		if(start_game == false) {
			interfaz.dibujarPresentacion();
			
			if(this.entorno.sePresiono(this.entorno.TECLA_ENTER))
					start_game = true;
					
			
		}
		return start_game;
	}
	
	private void controlDeNiveles() {
		
		if(isSiguiente_nivel()) {
			puntaje = 0;
			vidas_totales = 3;
			GAME_OVER = false;
			
			//
			if(getNivel()+1 <= getNivelesMaximos()) {
				setNivel(getNivel()+1);
			 
			}else {
				setNivel(1); // reset nivel
			}
			reiniciarObjetos();
			return;
		}
	}
	



	private void controlYDibujadoDeNuestroPersonaje() {
		
		
		if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && this.princesa.isSalta() == false) 
		{
			this.princesa.moverIzquierda(velocidadMovimiento);
		}
		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && this.princesa.isSalta() == false) 
		{
			this.princesa.moverDerecha(velocidadMovimiento);
		}	
		
		if(this.entorno.estaPresionada(this.entorno.TECLA_ARRIBA) 
				&& princesa.isSalta() == false  && isReiniciarNivel() == false) 
		{
			princesa.setSalta(true);
		
		}
		else if(princesa.isSalta() == true)
		{
			this.princesa.subir();
			
		}
		
		this.princesa.dibujar(this.entorno);

	}
	
	private void habilidadDeBolaDeFuego() {
		
		
		if(startDelayDisparo) {
			if(delayDisparo < TIEMPO_REUTILIZACION_DISPARO) {
				delayDisparo ++;
			}else {
				startDelayDisparo = false;
				delayDisparo = 0;
			}
		}
		
		
		if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && delayDisparo == 0) 
		{
			
			if(bolasDeFuego.size() < maximoDisparosVisibles) {
				
				BolaFuego bf = new BolaFuego(princesa.getX(), princesa.getY(), velocidadNivel * 1.3, entorno);
				bf.setMove(true);
				bolasDeFuego.add(bf);
			}

			startDelayDisparo = true;

			
		}
		
		// dibujamos las bolas, que existan
		for(BolaFuego bf : getBolasDeFuego()) {
			
			// guarda para evitar calculos innecesarios
			if(bf !=null) {
				if(bf.isMove()){
					bf.dibujar(entorno);
					bf.mover();
				}
			}
		}
	}
	
	private void movimientoDeObjetos() {
		
		movimientoEntidades.moverObstaculos(getObstaculos(), entorno, princesa, getVelocidadNivel());
		movimientoEntidades.movimientoSoldados( getSoldados(), entorno, princesa, this, velocidadSoldados, velocidadSoldadosInicial );	
		movimientoEntidades.moverItems(getItems(), entorno, princesa,  getVelocidadNivel());
				
	}
	
	
	private void colisiones() {
		
		
		//
		 colisiones.removerBolasDeFuego(getBolasDeFuego(), princesa, entorno);
		
		 if(colisiones.colisionObstaculosConPrincesa(getObstaculos(), princesa, entorno))
			 seMurio();
	 
		
							// COLISION SOLDADOS
		
		 if(colisiones.colisionSoldadosConPrincesa(getSoldados(), princesa, entorno))
			seMurio();
		
	
						// COLISION SOLDADOS  Y BOLA FUEGO
		
		 if(colisiones.colisionSoldadosConBolaDeFuego(getSoldados(), getBolasDeFuego(), entorno, princesa, sonido, this, velocidadSoldados, velocidadSoldadosInicial))
			  puntaje+=puntajePorSoldado;
		
		 
		 if(colisiones.colisionItems(getItems(), princesa, entorno)) {
		
			 	sonido.reproducirSonidoMoneda();
			    puntaje+=puntajePorItem;
		 }
		 
	}
	



	private void logicaDeMuerte() {
		
		
		// caida de princesa hacia abajo, cuando muere
				if(isReiniciarNivel()) {
					princesa.setY(princesa.getY()+3);
				}
				
				if(vidas_totales ==  0) {
					
					interfaz.setDibujarGameOver(true);
					GAME_OVER = true;
					setVelocidadNivel(0);
					 
					for(int i = 0; i < getSoldados().size(); i++) 
						getSoldados().get(i).setVelocidadSoldados(0);
					
				}else {
					reiniciarObjetos();
				
				}
	}
	
	
	public void juegoGanadoOPerdido() {
		
	
	
				
		
		if(this.entorno.sePresiono(this.entorno.TECLA_ENTER) && GAME_OVER && vidas_totales == 0) {
			interfaz.setDibujarGameOver(false);
			reiniciarObjetos();
			puntaje = 0;
			vidas_totales = 3;
			GAME_OVER = false;
			
		}
		
		
	     if(puntaje >=PUNTAJE_PARA_SIGUIENTE_NIVEL) {
				
				setVelocidadNivel(0);
				for(int i = 0; i < getSoldados().size(); i++) 
					getSoldados().get(i).setVelocidadSoldados(0);
				
				interfaz.juegoTerminado();
			}
	}
	
	
	public void seMurio() {
		
		
	
		if(sonido.isSoundDead() == false) {
	            sonido.setSoundDead(true);
	            sonido.reproducirSonidoMuerte();	
	     } 

		
		setVelocidadNivel(0);
		
		for(int i = 0; i < getSoldados().size(); i++) 
			getSoldados().get(i).setVelocidadSoldados(0);	
		
		princesa.setEstaMuerta(true);
		
		
		// esperar un poco, para reiniciar nivel
		
		if(vidas_totales > 0 && isReiniciarNivel() == false) {
		   vidas_totales -=1;
		  setReiniciarNivel(true);
		   sonido.setSoundDead(false);
		}else {
			GAME_OVER = true;

		}
		
	}
    
    
	
	
	


	// Setters y getters

   public boolean isStart_game() {
			return start_game;
   }

		
	public int getPuntajeSiguienteNivel( ) {
		return PUNTAJE_PARA_SIGUIENTE_NIVEL;
	}


	public ArrayList<Soldado> getSoldados() {
		return soldados;
	}

	

	public  ArrayList<Obstaculo> getObstaculos() {
		return obstaculos;
	}


	public ArrayList<BolaFuego> getBolasDeFuego() {
		return bolasDeFuego;
	}



	public void setVelocidadSoldadosInicial(int velocidadSoldadosInicial) {
		this.velocidadSoldadosInicial = velocidadSoldadosInicial;
	}


		

	public void setVelocidadNivel(int velocidadNivel) {
		this.velocidadNivel = velocidadNivel;
	}


	public int getVelocidadNivelInicial() {
		return velocidadNivelInicial;
	}




	public void setVelocidadNivelInicial(int velocidadNivelInicial) {
		this.velocidadNivelInicial = velocidadNivelInicial;
	}




	public ArrayList<Item> getItems() {
		return items;
	}



	public int getVelocidadNivel() {
		return velocidadNivel;
	}

	public void setSiguiente_nivel(boolean siguiente_nivel) {
	this.siguiente_nivel = siguiente_nivel;
	}



	public boolean isReiniciarNivel() {
		return reiniciarNivel;
	}




	public void setReiniciarNivel(boolean reiniciarNivel) {
		this.reiniciarNivel = reiniciarNivel;
	}

	public int getNivelesMaximos() {
		return nivelesMaximos;
	}


	

	private int getNivel() {
		// TODO Auto-generated method stub
		return nivel;
	}


	public void setNivel(int nivel) {
		this.nivel = nivel;
	}


	public void setNivelesMaximos(int nivelesMaximos) {
		this.nivelesMaximos = nivelesMaximos;
	}

	public boolean isSiguiente_nivel() {
		return siguiente_nivel;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
