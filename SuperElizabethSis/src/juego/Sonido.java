package juego;

import entorno.Herramientas;

public class Sonido {
	
private boolean soundDead = false;
private String deadSound = "dead2.wav";
private String coinSound = "coin.wav";
private String musicaFondo = "background2.wav";
private String deadSoldadoSound = "soldier.wav";

private String juegoSound = ""; 


public void reproducirSonidoMuerte() {

	Herramientas.play(deadSound);
}

public void reproducirSonidoMuerteSoldadito() {
	Herramientas.play(deadSoldadoSound);
}

public void reproducirMusicaFondo() {
	Herramientas.loop(musicaFondo);
}

public void reproducirSonidoMoneda() {
	Herramientas.play(coinSound);
}

public boolean isSoundDead() {
	return soundDead;
}


public void setSoundDead(boolean soundDead) {
	this.soundDead = soundDead;
}

}
