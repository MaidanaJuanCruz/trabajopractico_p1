package juego;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;

import entorno.Entorno;

public class Colisiones {
	
	/*
	 * 
	 * ColisioCoordenadas
	 * 
	 * Dibujamos un rectángulo en el sprite, y otro rectángulo en el sprite a colisionar
	 * comprobamos si hay intercesión por medio del metodo de la clase Rectángulo intersects, 
	 * dibujamos el rectángulo en la esquina superior izquierda tanto en x e y para los sprites, 
	 * con un ancho y alto del rectángulo, dependiendo del sprite, con una pequeña diferencia de desfase del 
	 * 1/2, aproximadamente.
	 */

	
	
	public  void removerBolasDeFuego(ArrayList<BolaFuego> bolasDeFuego, Princesa princesa, 
			Entorno entorno) {
		
		// si esta fuera del nivel eliminamos
		for(int i = 0; i < bolasDeFuego.size(); i++) {
			
			if(bolasDeFuego.get(i).getX() >= entorno.ancho() + bolasDeFuego.get(i).getAncho()/2) {
				bolasDeFuego.remove(i);
				break;
			}
			
			
		}

	
	}
	
	
	public  boolean colisionObstaculosConPrincesa(ArrayList<Obstaculo> obstaculos, 
			Princesa princesa, Entorno entorno) {
		
		
		
		 
		for(int i = 0; i < obstaculos.size(); i++) {
			
			 
			

		if(colisionCoordenadas(
				princesa.getX() - princesa.getAncho() /2,
				princesa.getY() - princesa.getAlto() /2, 
				princesa.getAncho(),
				princesa.getAlto() , 
				obstaculos.get(i).getX() - obstaculos.get(i).getAncho() /2, 
				obstaculos.get(i).getY() - obstaculos.get(i).getAlto() / 2, 
				obstaculos.get(i).getAncho(),
				obstaculos.get(i).getAlto() - obstaculos.get(i).getAlto() / 2)) {
		
		//obstaculos.get(i).getAlto() - obstaculos.get(i).getAlto() / 2, para evitar que nos mate su propulsor del obstaculo movil
			princesa.setEstaMuerta(true);
			
			// esperar un poco, para reiniciar nivel
			
			return true; // hay colision, devolvemos true
		}
		
		}
		
		return false;
		
	}
	
	
	public  boolean colisionItems(ArrayList<Item> items, 
			Princesa princesa, Entorno entorno) {
		
		
		
		 
		 
		for(int i = 0; i < items.size(); i++) {
			
	
	

		if(colisionCoordenadas(
				princesa.getX() - princesa.getAncho() /2,
				princesa.getY() - princesa.getAlto() /2, 
				princesa.getAncho(),
				princesa.getAlto(), 
				items.get(i).getX() - items.get(i).getAncho() /2, 
				items.get(i).getY() - items.get(i).getAlto() /2, 
				items.get(i).getAncho(),
				items.get(i).getAlto())) {
		
		
			// si colisiona la trasladamos fuera de la pantalla
			items.get(i).setMove(false); // no lo movemos mas
			items.get(i).setX(entorno.ancho() + items.get(i).getAncho()/2+200); 
			
 			
			return true; // hay colision, devolvemos true
		}
		
		}
		
		return false;
		
	}
	
	
	
	
	
	
	
	
	public  boolean colisionSoldadosConPrincesa(ArrayList<Soldado> soldados, 
			Princesa princesa, Entorno entorno) {
		
		
	
		 for(int i = 0; i < soldados.size(); i++) {
			

				// esquina superior izquierda del sprite en princesa.getX() - princesa.getAncho() /2, 

			if(colisionCoordenadas(
					princesa.getX() - princesa.getAncho() /2, 
					princesa.getY() - princesa.getAlto() / 2, 
					princesa.getAncho(),
					princesa.getAlto(), 
					soldados.get(i).getX() - soldados.get(i).getAncho() / 2,
					soldados.get(i).getY() - soldados.get(i).getAlto() / 2, 
					soldados.get(i).getAncho(),
					soldados.get(i).getAlto()) && 
					soldados.get(i).isMuerto() == false) {
			
				
				princesa.setEstaMuerta(true);
				
				
				return true; 
			}
			
		}

return false;
		
	}
	
	
	
	
	
	
	public  boolean colisionSoldadosConBolaDeFuego(ArrayList<Soldado> soldados, 
			ArrayList<BolaFuego> bolasDeFuego, Entorno entorno, Princesa princesa, Sonido sonido, 
			Juego juego, int velocidadSoldados, int velocidadSoldadosInicial ) {


		
	for(int i = 0; i < bolasDeFuego.size(); i++) {
			

			
			
			for(int s = 0; s < soldados.size(); s++) {
				
				
				
				
				 
			
				if(colisionCoordenadas(
						bolasDeFuego.get(i).getX()-bolasDeFuego.get(i).getAncho() /2,
						bolasDeFuego.get(i).getY()-bolasDeFuego.get(i).getAlto() /2, 
						bolasDeFuego.get(i).getAncho(),
						bolasDeFuego.get(i).getAlto(), 
						soldados.get(s).getX() - soldados.get(s).getAncho()/2,
						soldados.get(s).getY() - soldados.get(s).getAlto()/2, 
						soldados.get(s).getAncho(),
						soldados.get(s).getAlto()) && 
						soldados.get(s).isMuerto() == false &&
						soldados.get(s).isMove() &&
					    bolasDeFuego.get(i).isMove()) {
			
					
					sonido.reproducirSonidoMuerteSoldadito();

					bolasDeFuego.remove(i);// REMOVEMOS LA BOLA DE FUEGO COLISIONADA
					soldados.get(s).setMove(false);
					soldados.get(s).setDibujarMuerte(true, soldados.get(s).getX(), soldados.get(s).getY());
 					
					
				    
				    return true;
				  
				    
				    // cuando termine la animacion de la muerte, elimianos el soldado
				}else if(soldados.get(s).isEliminarAhora()){
					
					
					
					Soldado nuevo = new Soldado(entorno, princesa, juego, velocidadSoldados, velocidadSoldadosInicial);
					soldados.remove(s); // REMOVEMOS EL SOLDADO QUE HA COLISIONADO CON LA BOLA
					nuevo.setMove(false);
					soldados.add(s, nuevo);
				 
					   
				    return false;
					
				}
			}
			
			
			
		}
	
	

	
	return false;
	}
	

	
	
	

	public   boolean colisionCoordenadas(double obj2X, double obj2Y,
									   double obj2Acho, double obj2Alto,
									   double targetX, double targetY, 
									   double targetAncho, double targetAlto) {
		
		// restar un poco del contorno
		double  restarContornoAnchoTarget = targetAncho /2;
		double  restarContornoAnchoObj1 = obj2Acho /2;

		
		Rectangle obj = new Rectangle();
		obj.setBounds((int)targetX, (int)targetY, (int)(targetAncho -restarContornoAnchoTarget), (int)targetAlto);
		
		Rectangle obj2 = new Rectangle();
		obj2.setBounds((int)obj2X, (int)obj2Y, (int)(obj2Acho - restarContornoAnchoObj1), (int)obj2Alto);
		
		// si el rectangulo del jugador, hace colision con el rectangulo de un objeto, 
		// entonces hay intersección
		if(obj2.intersects(obj)) {
			return true;
		}else {
			return false;
		}
	
	
	}

}
