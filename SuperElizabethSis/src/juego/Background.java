package juego;

import java.awt.Image;
 

import entorno.Entorno;
import entorno.Herramientas;

public class Background {
	private int x; 
	private int y;
	private Entorno entorno;
	private Image imagen;
	private String fondoOSuelo; 
	
	public Background(int x, int y, Entorno entorno, int nivel, String fondoOSuelo) {
		this.x = x; 
		this.y = y;
		this.entorno = entorno;
		this.fondoOSuelo = fondoOSuelo;
		
		if(this.fondoOSuelo.equals("fondo"))
			 this.imagen = Herramientas.cargarImagen("fondoNivel_"+nivel+".png");
		else
			 this.imagen = Herramientas.cargarImagen("sueloNivel_"+nivel+".png");

	}


	public void mover(float velocidad) {
		
		
      	if(this.fondoOSuelo.equals("suelo")) {
				
				  x -=velocidad;
				
			}
   
 
	}
	 
	public void dibujar() {
		
		if(this.fondoOSuelo.equals("suelo"))
			entorno.dibujarImagen(imagen, x, y, 0);// fondo con moviento
		else
			entorno.dibujarImagen(imagen, entorno.ancho()/2, entorno.alto()/2, 0);// fondo sin moviemiento

	}
	
	public int getX() {
		return x;
	}



	public void setX(int x) {
		this.x = x;
	}

	public Image getImagen() {
		return imagen;
	}



	
}
