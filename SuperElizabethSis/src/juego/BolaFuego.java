package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class BolaFuego {
	
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	


	
	private Image imagen;
	private boolean isMove;
	private Entorno entorno;



	//constructor
	public BolaFuego(double x, double y, double velocidad, Entorno entorno) {
	
		super();
		this.x = x;
		this.y = y;
		this.entorno = entorno;
		this.velocidad=velocidad;
		this.imagen= Herramientas.cargarImagen("bola.png");
		this.ancho = imagen.getWidth(null);
		this.alto = imagen.getHeight(null);
	}


	public void dibujar(Entorno e) 
	{
		if(isMove())
			e.dibujarImagen(imagen, this.x, this.y,0);
	}
	
	public void mover() 
	{
		if(isMove()) {
			this.x+=velocidad;
		 
		}
			
			
	}
	



	public double getX() {
		return x;
	}


	public void setX(double x) {
		this.x = x;
	}


	public double getY() {
		return y;
	}


	public void setY(double y) {
		this.y = y;
	}


	public double getAncho() {
		return ancho;
	}


	public void setAncho(double ancho) {
		this.ancho = ancho;
	}


	public double getAlto() {
		return alto;
	}


	public void setAlto(double alto) {
		this.alto = alto;
	}



	public boolean isMove() {
		return isMove;
	}


	public void setMove(boolean isMove) {
		this.isMove = isMove;
	}

}
