package juego;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Interfaz {
	
	
	
	private int posX; 
	private int posY;

	private Entorno entorno;
	private boolean dibujarGameOver;

	
	
	private Image presentacion;

	private Image fondoJuegoGanado;
	private Image princesa;

	
	private Image [] imagenesRey;
	private int totalImagenesRey = 15;
	private int imagenActualRey = 0;
	private int contadorTickRey = 0;
	
	
	private Image [] imagenesGato;
	
	private Image  [] uiInstrucciones;
	
	private int totalImagenesGato = 10;
	private int imagenActualGato = 0;
	private int contadorTickGato = 0;
	private int movimientoGatoDerecha = 0;
	
	private int pausa = 240;
	private int pausa_tras_juego_ganado = pausa;
	
	private Juego juego;
	
	public Interfaz(int posX, int posY, Entorno entorno, Juego juego) {
		
		this.juego = juego;
		this.entorno = entorno;
		this.posX = posX;
		this.posY = posY;
	
		this.presentacion = Herramientas.cargarImagen("presentacion.png");
		this.uiInstrucciones = new Image[2];
		this.uiInstrucciones[0]= 	Herramientas.cargarImagen("ui_fire.png");
		this.uiInstrucciones[1] = Herramientas.cargarImagen("ui_saltar.png");
		
		fondoJuegoGanado = Herramientas.cargarImagen("fondo_juego_gana.png");
		princesa = Herramientas.cargarImagen("princesa_left.png");
		
		
		
		
		imagenesRey = new Image[totalImagenesRey];
	
		
		for(int i = 0; i < imagenesRey.length; i ++) {
			
			
			// Herramientas al preceser no carga rutas absolutas
			
				this.imagenesRey[i]  = Herramientas.cargarImagen("king"+"_"+i+".png");
			
		
		}
		
		
	imagenesGato= new Image[totalImagenesGato];
	
		
		for(int i = 0; i < imagenesGato.length; i ++) {
			
			
			// Herramientas al preceser no carga rutas absolutas
			
				this.imagenesGato[i]  = Herramientas.cargarImagen("cat"+"_"+i+".png");
			
		
		}
		
	}
	
	public void dibujarPresentacion() {
		
		if(juego.isStart_game() == false) {
			entorno.dibujarImagen(presentacion, entorno.ancho()/2, entorno.alto()/2, 0);
		}
	}

	// pausa de juego ganaod
	public void juegoTerminado() {
		
		if(pausa_tras_juego_ganado < 0) {
			 DibujarJuegoGanado();
		}else {
		
			if(pausa_tras_juego_ganado > 0) {

				entorno.cambiarFont("Nivel Completado!", 50,Color.BLUE);
				entorno.escribirTexto("Nivel Completado!",200, entorno.alto()/2);
			}
			pausa_tras_juego_ganado --;
		}
		
	}
	public void DibujarJuegoGanado() {
		

			
			entorno.dibujarImagen(fondoJuegoGanado, entorno.ancho()/2, entorno.alto()/2, 0);
			
			
			
			contadorTickRey +=1;
			// vamos intercalando las imagenes
			if(contadorTickRey % 30 == 0) {
				if(imagenActualRey < totalImagenesRey-1) {
					imagenActualRey ++;
				} 
			}
			entorno.dibujarImagen(imagenesRey[imagenActualRey], 155,342,0);
			//e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.RED);
			
			
			
			contadorTickGato +=0.1;
			// vamos intercalando las imagenes
			if(contadorTickGato % 20 == 0) {
				if(imagenActualGato < totalImagenesGato-1) {
					imagenActualGato ++;
				}else {
					imagenActualGato = 0;
				}
			}
			
			// que camine hasta la princesa
			if(435+movimientoGatoDerecha < 650)
				movimientoGatoDerecha ++;
				

			entorno.dibujarImagen(imagenesGato[imagenActualGato], 435+movimientoGatoDerecha,390,0);
			//e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.RED);
			
			entorno.dibujarImagen(princesa, 720,348,0);
			
			
			if(imagenActualRey == imagenesRey.length-1) {
				
				dibujarTxtJuegoGanado();
			}
		
		
	}
	
	public void dibujarTxtJuegoGanado() {
		
		if(entorno.sePresiono(this.entorno.TECLA_ENTER)){
			
			pausa_tras_juego_ganado = pausa; // reset 
			imagenActualRey = 0;// para volver a animar en otro nivel
			movimientoGatoDerecha = 0;
			juego.setSiguiente_nivel(true);
			
	  	}
		
		entorno.cambiarFont("Rey Camir derrotado", 50,Color.YELLOW);
		entorno.escribirTexto("Rey Camir derrotado",150, entorno.alto()/2-100);
		
		entorno.cambiarFont("Enter para nuevo nivel", 30,Color.CYAN);
		entorno.escribirTexto("Enter para un nuevo nivel", 220,  entorno.alto()/2 +50);
	}
	
	
	public void DibujarInterfaz(int puntaje, int vidas) {
		
		entorno.dibujarImagen(uiInstrucciones[0], entorno.ancho()/2 - 100, entorno.alto()-20, 0, 0.5);
		entorno.dibujarImagen(uiInstrucciones[1], entorno.ancho()/2+50, entorno.alto()-20, 0, 0.5);

		entorno.cambiarFont(""+vidas, 25,Color.YELLOW);
		entorno.escribirTexto("Vidas: "+vidas,posX, posY);
		
		
		entorno.escribirTexto("Puntaje: "+puntaje+"/"+juego.getPuntajeSiguienteNivel(), entorno.ancho()/2+100, posY);
		
		entorno.cambiarFont("GAME OVER", 54,Color.RED);
		
		if(dibujarGameOver) {
			entorno.escribirTexto("GAME OVER", 200, entorno.alto()/2);
			entorno.cambiarFont("ENTER PARA VOLVER A JUGAR", 36,Color.CYAN);
			entorno.escribirTexto("ENTER PARA VOLVER A JUGAR", 100, entorno.alto()/2 +100);
		}
	}
	
	
	public void setDibujarGameOver(boolean dibujarGameOver) {
		this.dibujarGameOver = dibujarGameOver;
	}

	
	
	
}
