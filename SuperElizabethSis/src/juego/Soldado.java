package juego;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {
	
	private double x;
	private double y;
	private double ancho;
	private double alto;
	
	private boolean isMove = false;
	private Entorno entorno;
	
	private Image [] imagenes;
	private Image [] imagenesMuerte;
	
	

	private int totalImagenesRun = 12;
	private int imagenActualRun = 0;
	private int contadorTickRun = 0;
	
	private   float velocidadSoldados = 4;



	private  float velocidadSoldadosInicial = velocidadSoldados;

	private int totalImagenesMuerte = 9;


	private int imagenActualMuerte = 0;
	private int contadorTickMuerte = 0;
	private boolean muerto = false;



	private double posXMuerte = 0;
	private double posYMuerte = 0;
	
	
	// salto
	private int distanciaDesdeSalto = 100;
	private int distanciaFueraNivel = 200;
	private boolean salta = false;
	private boolean isSubiendo = true;
	private boolean isCayendo = false;
	private double gravedad = 4.8;// un valor alto es mas rapido
	private static final double GRAVEDAD_INICIAL =4.8;// un valor alto es mas rapido
	int contadorTick = 0; // 120tick = 1seg
	private int velocidadSalto = 1;
	private double velocidadCaida = 8;

	private static final double DISTANCIA_SALTO = 100; // es mejor que sea en pixeles
	private double distanciaSalto = 0;
	private boolean empezarSalto = false;
	private Princesa princesa;
	private Juego juego; 
	private boolean eliminarAhora = false;
	
	//constructor
	public Soldado(Entorno entorno, Princesa princesa, Juego juego, int velocidadSoldados, int velocidadSoldadosInicial) {
	 
		this.velocidadSoldados = velocidadSoldados; 
		this.velocidadSoldadosInicial = velocidadSoldadosInicial;
		this.entorno = entorno;
		this.princesa = princesa;
		this.juego = juego; 
		
		imagenes = new Image[totalImagenesRun];
		imagenesMuerte = new Image[totalImagenesMuerte];
		
		for(int i = 0; i < imagenes.length; i ++) {
			
			
			// Herramientas al preceser no carga rutas absolutas
			
				this.imagenes[i]  = Herramientas.cargarImagen("soldado"+"_"+i+".png");
			
		
		}
		
	  for(int i = 0; i < imagenesMuerte.length; i ++) {
			
			
			// Herramientas al preceser no carga rutas absolutas
			
				this.imagenesMuerte[i]  =  Herramientas.cargarImagen("soldado_muere"+"_"+i+".png");
		
		
		}
		
		
		
		
		this.ancho = imagenes[0].getWidth(null);
		this.alto = imagenes[0].getHeight(null);
		
		
		this.x = entorno.ancho()+distanciaFueraNivel;
		this.y = (princesa.getPrincesa().getAuxNormalPosY() + princesa.getPrincesa().getAlto() / 2) - alto/2;
	
		this.distanciaSalto = y - (alto/2) - DISTANCIA_SALTO;
	}
	
	
	
	//metodo mover
	public void mover() {
				
				// saltar obstaculos
	
				
				
				if(isMove && isMuerto() == false) {
					this.x -= velocidadSoldados;
					saltarObstaculo(); 

					subir(); // subimos si corresponde
				}else if(isMuerto()) {
					// para que la posicion de dibujado de la animacion de la muerte, 
					// sea igual al movimiento del piso
					this.posXMuerte -= juego.getVelocidadNivel();
					
				}
		 	
			}
     public void dibujarMuerte() {
			
			
			if(eliminarAhora == true)
				return;
	
			contadorTickMuerte +=1;
			// vamos intercalando las imagenes
			if(contadorTickMuerte % 10 == 0) {
				if(imagenActualMuerte < totalImagenesMuerte-1) {
					imagenActualMuerte ++;
				}
			}
			
			
			
			if(imagenActualMuerte < totalImagenesMuerte) {
				entorno.dibujarImagen(imagenesMuerte[imagenActualMuerte], posXMuerte, posYMuerte, 0);
			}
			if(imagenActualMuerte ==totalImagenesMuerte-1)
				eliminarAhora = true;
				
		}

	


	public boolean isEliminarAhora() {
		return eliminarAhora;
	   }


		public void setDibujarMuerte(boolean muerto, double xMuerte, double yMuerte) {
			this.posXMuerte = xMuerte;
			this.posYMuerte = yMuerte;
			this.muerto = muerto;
		}

	
		
		//metodo dibujar
		public void dibujar(Entorno e) 
		{
			
			if(isMuerto() == false) {
				contadorTickRun +=1;
			// vamos intercalando las imagenes
				if(contadorTickRun % 20 == 0) {
					if(imagenActualRun < totalImagenesRun-1) {
					imagenActualRun ++;
					}else {
					imagenActualRun = 0;
					}
				}
				e.dibujarImagen(imagenes[imagenActualRun], this.x,this.y,0);
			
			
			}else {
				
				dibujarMuerte();
			}
 		}
		
		
		
		public void caer() {
			// si esta cayendo, y incrementa hasta tocar el suelo
			
			// incrementamos la velocidad de caida pero aumentado la gravedad en cada seg
			this.y += (velocidadCaida*gravedad);
			this.x -=(velocidadSalto);
			if(this.y > princesa.getPrincesa().getAuxNormalPosY()) {
			   gravedad  = GRAVEDAD_INICIAL;
			   contadorTick = 0;
			   isSubiendo = true; // reset variables
			   isCayendo = false;// reset variables
			   y  =  (princesa.getPrincesa().getAuxNormalPosY() + princesa.getPrincesa().getAlto() / 2) - alto/2; // posicion inicial princisa
			   salta = false; // ya toco el suelo, no salta. 
			   empezarSalto = false; // para que vuelva a empezar
			}
		}
		
		
		
		
	public void subir() {

		// estamos saltando!
		if(salta) {
			/* 
			  la fuerza de gravedad es fuerte al principio, 
			 a medida que subimos vamos decrementando su valor,
			 a medida que el peso del cuerpo ejerce la gravedad hacia abajo
			 
			 */
			
			if(contadorTick < 120 && gravedad > 0 && isSubiendo) { 
			
				 contadorTick++;
				 gravedad -=0.01;
				
				 
			/*
			 * La fuerza de gravedad al estar la princisa por caer luego del salto, es casi 0, 
			 * entonces aumentamos la gravedad, simulando el peso de un cuerpo
			 * 	 
			 */
			}else if(contadorTick < 120 && gravedad < GRAVEDAD_INICIAL && isCayendo) {
				
				 contadorTick++;
				 gravedad +=0.01;
				
				
			}
			
			
				
				// si y es mayor distanciaSalto, entonces vamos subiendo, hasta que alcanzemos la distancia 
				// de salto
			
			// subimos!	
			if(y > distanciaSalto  && isSubiendo == true) {
			 
				// subimos, considerando la gravedad en aumento y llegando a disminur al final
				this.y -=(velocidadSalto*gravedad);
				this.x -=(velocidadSalto);
				
			// caemos!	
			}else {
				
				// al caer  setiamos la gravedad en 0, es ese instante en que el cuerpo llega a una 
				// aceleracion de 0
				if(isCayendo == false) {
					isCayendo = true; // praa no volver a entrar en la condicion
					  gravedad  = 0; // reset gravedad, estamos por caer, la gravedad es 0
					  contadorTick = 0; 
				}
					
				isSubiendo = false; // ya no sube
				caer();
				
			}
			
			 
			} 
			
		}
		
		
		
		private void saltarObstaculo() {
		
		 for(int i = 0; i < juego.getObstaculos().size(); i ++) {
		
			 // solo satar� los fijos
			 if(juego.getObstaculos().get(i).getTipo().equals("fijo")) {
				 
				//if((x + ancho / 2) > 0 && x < entorno.ancho() + ancho
				if((x + ancho / 2) > 0 && (x  + ancho/2 < entorno.ancho()) ) {
					double  distanciaSalto = x -  juego.getObstaculos().get(i).getX();
					if( distanciaSalto  > 0 && distanciaSalto < distanciaDesdeSalto) {
							
							if(empezarSalto == false) {
								empezarSalto = true;
								salta = true;
							}
						 
				
					}
				}
			 }
		 }
			
		}
		
		public double getX() {
			return x;
		}


		public void setX(double x) {
			this.x = x;
		}


		public double getY() {
			return y;
		}


		public void setY(double y) {
			this.y = y;
		}


		public double getAncho() {
			return ancho;
		}


		public void setAncho(double ancho) {
			this.ancho = ancho;
		}


		public double getAlto() {
			return alto;
		}


		public void setAlto(double alto) {
			this.alto = alto;
		}

		
		public boolean isMove() {
			return isMove;
		}


		public void setMove(boolean isMove) {
			this.isMove = isMove;
		}
		
		public float getVelocidadSoldados() {
			return velocidadSoldados;
		}


		public void setVelocidadSoldados(float velocidadSoldados) {
			this.velocidadSoldados = velocidadSoldados;
		}


		public float getVelocidadSoldadosInicial() {
			return velocidadSoldadosInicial;
		}


		public void setVelocidadSoldadosInicial(float velocidadSoldadosInicial) {
			this.velocidadSoldadosInicial = velocidadSoldadosInicial;
		}
		
		public boolean isMuerto() {
			return muerto;
		}


		public void setMuerto(boolean muerto) {
			this.muerto = muerto;
		}
 
}
