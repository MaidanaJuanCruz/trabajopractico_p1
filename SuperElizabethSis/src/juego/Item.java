package juego;

import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Item {
	
	
	
	private double x;
	private double y;
	private double ancho;
	private double alto;

	private boolean isMove;
	private Entorno entorno; 
	
	private Image [] imagenes;
	private int totalImagenes = 7;
	private int imagenActual = 0;
	private int contadorTick = 0;
	
	public Item (double x, Princesa princesa, 
			boolean isMove, Entorno entorno) {
		
		
		
		this.x = x;
		this.entorno = entorno;
		this.isMove = isMove;
		
		imagenes = new Image[totalImagenes];
	
		
		for(int i = 0; i < imagenes.length; i ++) {
			
			
			// Herramientas al preceser no carga rutas absolutas
			
				this.imagenes[i]  = Herramientas.cargarImagen("coin"+"_"+i+".png");
			
		
		}
		
		this.ancho = imagenes[0].getWidth(null);
		this.alto = imagenes[0].getHeight(null);
		
		Random rn = new  Random();
		int variar = rn.nextInt(50);
		
		y = entorno.alto()/2+variar;// entre 100  y 400; 
		
	 }
	


	//metodo dibujar
	public void dibujar(Entorno e) 
	{
		
		contadorTick +=1;
		// vamos intercalando las imagenes
		if(contadorTick % 10 == 0) {
			if(imagenActual < totalImagenes-1) {
				imagenActual ++;
			}else {
				imagenActual = 0;
			}
		}
		e.dibujarImagen(imagenes[imagenActual], this.x,this.y,0);
		//e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.RED);
	}
	
	
	//metodo mover
	public void mover(float velocidad) 
	{
		
		if(isMove) {
			this.x -= velocidad;
		
		}
	
	
		
	}
	
	
	public void setX(double x) {
		this.x = x;
	}


	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	
	public boolean isMove() {
		return isMove;
	}

	public void setMove(boolean isMove) {
		this.isMove = isMove;
	}
	
	}
	
