package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
	
	private int distanciaFueraNivel = 100;
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private boolean isMove;
	private Entorno entorno; 

	// obstaculo fijo

	private Image imagen;
	private String  imageName;
	
	// obstaculo movil
	private String tipo;
	private Image [] imagenesObstaculoMovil;
	private int totalImagenesObstaculoMovil = 15;
	private int imagenActualObstaculoMovil = 0;
	private int contadorTickimagenesObstaculoMovil = 0;

	private int limiteAltura;
	private boolean sube = true;
	private double velocidadDeSubidaYBajada = 1.5;
	private Princesa princesa;
	
	

	public boolean isMove() {
		return isMove;
	}


	public void setMove(boolean isMove) {
		this.isMove = isMove;
	}


	//constructor
	public Obstaculo(Princesa princesa, 
			boolean isMove, Entorno entorno, String tipo) {
		
		this.tipo = tipo;
		this.entorno = entorno;
		this.isMove = isMove;
		this.princesa = princesa;
		
		if(tipo.equals("fijo")) {
			
			this.imageName = getRandomNombreImagen();
			this.imagen = Herramientas.cargarImagen(imageName);
			this.ancho = this.imagen.getWidth(null);
			this.alto = this.imagen.getHeight(null);
			this.x = entorno.ancho()+distanciaFueraNivel;
			this.y = (princesa.getPrincesa().getAuxNormalPosY() + princesa.getPrincesa().getAlto() / 2) - alto/2;// desde donde pisa, restamos el alto
																
		}else {
			
			
			imagenesObstaculoMovil = new Image[totalImagenesObstaculoMovil];
			
			
			for(int i = 0; i < imagenesObstaculoMovil.length; i ++) {
				
				
				// Herramientas al processor no carga rutas absolutas
				
					this.imagenesObstaculoMovil[i]  = Herramientas.cargarImagen("robot"+"_"+i+".png");
				
			
			}
			
			Random rn = new  Random();
					
			
			this.limiteAltura = rn.nextInt(300)+100;// entre 100  y 300; 
			this.ancho = imagenesObstaculoMovil[0].getWidth(null);
			this.alto = imagenesObstaculoMovil[0].getHeight(null);
			
			this.x = entorno.ancho()+distanciaFueraNivel;
			this.y = (princesa.getPrincesa().getAuxNormalPosY() + princesa.getPrincesa().getAlto() / 2) - alto/2; // posicion inicial princisa
			
		}

		
		
		
	}


	

	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	// random obstaculos
	public String getRandomNombreImagen() {
		Random rn = new Random();
		 int  randomImg = rn.nextInt(4)+1;
		
		
		 switch(randomImg) {
		 
		 case 1: 
			 
			return "rock-monster_obs.png";
			 
		 case 2: 
			 
			 return "rock-obstacle_obs.png";
			 
		 case 3:
			 return "bloque.png";
			 
		 case 4:
			 return "sharp-wood.png";
		 default: 
			 return "rock-monster_obs.png";
		 
		 
		 }
		
	}
	
	//metodo dibujar
	public void dibujar(Entorno e) 
	{
			if(tipo.equals("fijo")) {
				e.dibujarImagen(imagen, this.x, this.y, 0);
			}else {
				
				contadorTickimagenesObstaculoMovil +=1;
				// vamos intercalando las imagenes
				if(contadorTickimagenesObstaculoMovil % 20 == 0) {
					if(imagenActualObstaculoMovil < totalImagenesObstaculoMovil-1) {
						imagenActualObstaculoMovil ++;
					}else {
						imagenActualObstaculoMovil = 0;
					}
				}
				e.dibujarImagen(imagenesObstaculoMovil[imagenActualObstaculoMovil], this.x,this.y,0);
				//e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.RED);
				
			}
	}
	
	//metodo mover
	public void mover(float velocidad) 
	{
		if(tipo.equals("fijo")) {
		 if(isMove) {
			this.x -= velocidad;
		
		 }
		}else {
		if(isMove) {
			this.x -= velocidad;
			
			if(y > limiteAltura && sube == true) {
				y-=velocidadDeSubidaYBajada;	
			}else {
				sube = false;
				y+=velocidadDeSubidaYBajada;
				
				if(y > (princesa.getPrincesa().getAuxNormalPosY() + princesa.getPrincesa().getAlto() / 2) - alto/2) {
					
					y =  (princesa.getPrincesa().getAuxNormalPosY() + princesa.getPrincesa().getAlto() / 2) - alto/2; // posicion inicial princisa
					sube = true;
				}
			 }
		  }
			
		}
	
		// si esta fuera dle nivel lo trasladamos adelante
		/*if(this.x  < - ancho / 2 ) {
		
			x = entorno.ancho() + ancho / 2; // trasladamos fuera del nivel hacia adelante
			
			isMove = false; // no lo movemos mas
		}
			*/
			
		
	}
	
	public void setX(double x) {
		this.x = x;
	}


	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	
	
	public String getTipo() {
		return tipo;
	}


}
