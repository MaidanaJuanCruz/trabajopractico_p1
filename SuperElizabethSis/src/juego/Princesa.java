package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	
	private int x;
	private int y;
	private double ancho;
	private double alto;

	private Image imagenMuerte;
	private Image [] imagenesPrincesaRun;

	private int totalImagenesPrincesaRun = 7;
	private int imagenActualPrincesaRun = 0;
	private int contadorTickImagenesPrincesaRun = 0;
	

	

	

	//private boolean mirarDer = true;
	
	private boolean salta = false;
	private int auxNormalPosY = 0; // para que quede en su primera posY al saltar
	private boolean isSubiendo = true;
	private boolean isCayendo = false;
	private double gravedad = 4.8;// un valor alto es mas rapido
	private static final double GRAVEDAD_INICIAL =4.8;// un valor alto es mas rapido
	int contadorTick = 0; // 120tick = 1seg
	private int velocidadSalto = 1;
	private double velocidadCaida = 8;


	private static final double DISTANCIA_SALTO = 100; // es mejor que sea en pixeles
	private double distanciaSalto = 0;
	private Entorno entorno;

	
	private boolean estaMuerta = false;
	private Juego juego;
	private boolean playSoundSalto = false;
	
	//constructor
	public Princesa(int x, int y,  Entorno entorno, 
			Juego juego) {
	 
		
		this.juego = juego;

		this.x = x;
		this.y = y;
		this.auxNormalPosY = y;
		
		
		imagenesPrincesaRun = new Image[totalImagenesPrincesaRun];
		for(int i = 0; i < imagenesPrincesaRun.length; i ++) {
			
			
			// Herramientas al preceser no carga rutas absolutas
			
				this.imagenesPrincesaRun[i]  = Herramientas.cargarImagen("princes"+"_"+i+".png");
			
		
		}
		this.entorno = entorno;
		this.imagenMuerte= Herramientas.cargarImagen("princes_die.png");
		
		
		this.ancho = imagenesPrincesaRun[0].getWidth(null);
		this.alto = imagenesPrincesaRun[0].getHeight(null);
	
	
		
		this.distanciaSalto = y - (alto/2) - DISTANCIA_SALTO;
	}
	
	
	//metodos
	public Princesa getPrincesa() {
		return this;
	}

	//getters
	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}


	public double getAncho() {
		return ancho;
	}


	public double getAlto() {
		return alto;
	}


		//metodo dibujar
		public void dibujar(Entorno e) 
		{
		
			if(estaMuerta == false) {
			
			contadorTickImagenesPrincesaRun +=1;
			// vamos intercalando las imagenes
			if(contadorTickImagenesPrincesaRun % 20 == 0) {
				if(imagenActualPrincesaRun < totalImagenesPrincesaRun-1) {
					imagenActualPrincesaRun ++;
				}else {
					imagenActualPrincesaRun = 0;
				}
			}
			e.dibujarImagen(imagenesPrincesaRun[imagenActualPrincesaRun], this.x,this.y,0);
		
			}else {
				// se murio, dibujamos sprite 
				e.dibujarImagen(imagenMuerte, this.x,this.y,0);

			
			}
		
		}
		
	
		
		public void subir() {
			
			
			
			// estamos saltando!
						if(salta) {
			/* 
			  la fuerza de gravedad es fuerte al principio, 
			 a medida que subimos vamos decrementando su valor,
			 a medida que el peso del cuerpo ejerce la gravedad hacia abajo
			 
			 */
			
			if(playSoundSalto == false) {
		    	 playSoundSalto = true;
				Herramientas.play("jump.wav");
			}
							
			if(contadorTick < 120 && gravedad > 0 && isSubiendo) { 
			
				 contadorTick++;
				 gravedad -=0.01;
				
				 
			/*
			 * La fuerza de gravedad al estar la princisa por caer luego del salto, es casi 0, 
			 * entonces aumentamos la gravedad, simulando el peso de un cuerpo
			 * 	 
			 */
			}else if(contadorTick < 120 && gravedad < GRAVEDAD_INICIAL && isCayendo) {
				
				 contadorTick++;
				 gravedad +=0.01;
				
				
			}
			
			
			
				
				// si y es mayor distanciaSalto, entonces vamos subiendo, hasta que alcanzemos la distancia 
				// de salto
			
			// subimos!	
			if(y > distanciaSalto  && isSubiendo == true) {
			 
				// subimos, considerando la gravedad en aumento y llegando a disminur al final
				this.y -=(velocidadSalto*gravedad);
				
				
			// caemos!	
			}else {
				
				// al caer  setiamos la gravedad en 0, es ese instante en que el cuerpo llega a una 
				// aceleracion de 0
				if(isCayendo == false) {
					isCayendo = true; // praa no volver a entrar en la condicion
					  gravedad  = 0; // reset gravedad, estamos por caer, la gravedad es 0
					  contadorTick = 0; 
				}
					
				isSubiendo = false; // ya no sube
				caer();
				
			}
			
			 
			} 
			
		}

	


		public void caer() {
			// si esta cayendo, y incrementa hasta tocar el suelo
			
			// incrementamos la velocidad de caida pero aumentado la gravedad en cada seg
			this.y += (velocidadCaida*gravedad);
			
			if(this.y > auxNormalPosY) {
			   gravedad  = GRAVEDAD_INICIAL;
			   contadorTick = 0;
			   isSubiendo = true; // reset variables
			   isCayendo = false;// reset variables
			   y  = auxNormalPosY; // posicion inicial princisa
			   salta = false; // ya toco el suelo, no salta. 
			   playSoundSalto = false;
			}
		}


		public void setY(int y) {
			this.y = y;
		}
		public void moverDerecha(double velocidadMovimiento) {
			
			if(x < entorno.ancho() / 2)
				this.x += velocidadMovimiento;
			//mirarDer = true;
			
		}

		public void moverIzquierda(double velocidadMovimiento) {
			 
			// limite para que no se vaya fuera del nivel
			if((x-ancho/2) > 0) {
				//mirarDer = false;
				this.x -= (velocidadMovimiento+juego.getVelocidadNivel()); // dif, por volver hacia atras 
			}
		}

		
		// para alinear los obstaculos  a la posicion inicial de la princesa
		public double getAuxNormalPosY() {
			return auxNormalPosY;
		}
		
		public void setX(int x) {
			this.x = x;
		}


		
		public boolean isSalta() {
			return salta;
		}


		public void setSalta(boolean salta) {
			this.salta = salta;
		}
		
	
	
		
		public void setEstaMuerta(boolean estaMuerta) {
			this.estaMuerta = estaMuerta;
		}

}
